if [ $# -eq 0 ]
then
  bucket=s3://om-innovation-website/
else
  bucket="s3://$1/"
fi
echo "Compiling"
hugo
echo "Done"
echo "Clean up s3 bucket ${bucket}"
aws s3 rm ${bucket} --recursive
echo "Upload to s3 bucket ${bucket_name}"
aws s3 cp public ${bucket} --recursive --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
echo "Deploy done"


